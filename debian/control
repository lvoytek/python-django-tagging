Source: python-django-tagging
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Jonas Genannt <genannt@debian.org>,
 Christopher Baines <mail@cbaines.net>,
 Christos Trochalakis <ctrochalakis@debian.org>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
Build-Depends-Indep:
 dh-python,
 python-django-doc,
 python3-all,
 python3-django,
 python3-docutils,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
Rules-Requires-Root: no
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-django-tagging
Vcs-Git: https://salsa.debian.org/python-team/packages/python-django-tagging.git
Homepage: https://github.com/Fantomas42/django-tagging

Package: python-django-tagging-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends}
Description: Generic tagging application for Django projects (Documentation)
 This is a generic tagging application for Django, which allows
 association of a number of tags with any Model instance and makes
 retrieval of tags simple.
 .
 Models can be expanded with a new TagField; web templates
 can easily include tag clouds and dedicated views are available
 to browse objects by tag.
 .
 This package contains the documentation.

Package: python3-django-tagging
Architecture: all
Depends:
 python3-django,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-django-tagging-doc,
Description: Generic tagging application for Django projects (Python 3)
 This is a generic tagging application for Django, which allows
 association of a number of tags with any Model instance and makes
 retrieval of tags simple.
 .
 Models can be expanded with a new TagField; web templates
 can easily include tag clouds and dedicated views are available
 to browse objects by tag.
 .
 This is the Python 3 version of the package.
